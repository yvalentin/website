export const state = () => ({
  list: [
    {
      date: '2016',
      degree: 'Diplôme Etudiant-Entrepreneur (D2E)',
      institute: 'Université de Lyon',
      city: 'Lyon',
      link:
        'https://www.universite-lyon.fr/formation/formations-innovantes/d2e-diplome-d-etudiant-entrepreneur/diplome-d-etudiant-entrepreneur-d2e--903.kjsp'
    },
    {
      date: '2007',
      degree: 'DUT informatique',
      institute: 'UCBL - Institut universitaire de Lyon',
      city: 'Lyon',
      link:
        'https://iut.univ-lyon1.fr/formation/offre-de-formations/informatique-villeurbanne-doua-/dut-informatique-602480.kjsp'
    },
    {
      date: '2001',
      degree: 'Baccalauréat Série Scientifique',
      institute: 'Lycée Jean-Puy',
      city: 'Roanne',
      link: 'http://jean-puy.elycee.rhonealpes.fr/'
    }
  ]
})

export const mutations = {}
