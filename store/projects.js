export const state = () => ({
  list: [
    {
      name: 'LiveliHoods- UNHCR - CartONG',
      url: 'https://cartong.org/',
      categories: [
        'Symfony',
        'Api Platform',
        'Ansible',
        'Gitlab CI/CD',
        'VueJs',
        'PostGreSQL',
        'Typescript',
        'Docker'
      ].sort(),
      visible: 1,
      img: 'livelihoods.cartong.org.webp',
      description:
        'Ops : \n' +
        '<br/>   - Mise en place du serveurs de preprod et environnement de dev sous docker\n' +
        '<br/>   - Déploiement automatisé avec Ansible et AWX - Gitlab CI/CD\n' +
        '<br/>' +
        'Dev :\n' +
        '<br/>   - Développement FullStack avec Api Platform et VueJs'
    },
    {
      name: "La bonne pioche - Ferme d'avenir",
      url: 'https://labonnepioche.fermesdavenir.org/',
      categories: ['React.js', 'NextJs', 'Ansible', 'DevOps', 'ExpressJs', 'PostGreSQL', 'Typescript'].sort(),
      visible: 1,
      img: 'lbp.webp',
      description:
        'Ops : \n' +
        '<br/>   - Mise en place des serveurs de preprod / prod\n' +
        '<br/>   - Déploiement automatisé avec Ansible et AWX\n' +
        '<br/>' +
        'Dev :\n' +
        '<br/>   - Maintenances évolutives'
    },
    {
      name: 'K3D',
      url: 'https://k3d.fr/',
      categories: ['Symfony', 'Api Platform', 'Ansible', 'VueJs', 'OAuth', 'DevOps', 'Typescript', 'Mysql'].sort(),
      visible: 1,
      img: 'k3d-sarl.webp',
      description:
        'Refonte du système d\'information, passage d\'un "from scratch" sous PHP 5.4 au framework Symfony 4.4 sous PHP 7.4\n' +
        'Micro services avec API Platform'
    },
    {
      name: 'Actuvélo',
      url: 'https://www.actuvelo.com/',
      categories: ['Wordpress', 'Roots.io', 'Ansible'].sort(),
      visible: 1,
      img: 'actuvelo.png',
      description: 'Refonte du site internet'
    },
    {
      name: 'Aventure et volcans',
      url: 'https://www.aventurevolcans.com/fr/',
      categories: ['Code Igniter', 'Ansible', 'JQuery', 'Bootstrap', 'Mysql'].sort(),
      visible: 1,
      img: 'aventurevolcans.png',
      description: 'Maintenance et évolution de la plateforme de voyage spécialisée dans les volcans'
    },
    {
      name: 'Frog In Nz - Assurance Auto',
      url: 'https://assurauto.frogs-in-nz.com/homepage',
      categories: ['Silex', 'Ansible', 'JQuery', 'Materialize', 'Mysql', 'PDF'].sort(),
      visible: 1,
      img: 'assurauto.frogs-in-nz.png',
      description: "Développement d'un module d'assurance en ligne avec paiement, maintenance et évolution"
    },
    {
      name: 'Institut des ressources industrielles',
      url: 'https://www.iri-lyon.com/',
      categories: ['Symfony', 'AngularJS', 'Ansible', 'Material Design', 'Mysql'].sort(),
      visible: 1,
      img: 'iri.png',
      description: 'Maintenance et évolution de la plateforme de formation'
    },
    {
      name: 'Comptoir Des Plats',
      categories: ['Symfony', 'Ansible', 'Material Design', 'Mysql'].sort(),
      visible: 1,
      img: 'cdp.webp',
      description:
        'Création de la plateforme – Symfony 2.8 – Material design – Bower/Grunt - Vagrant / Ansible\n' +
        'Finaliste à Lyon Start Up\n' +
        'Gestion de projet entrepreneurial'
    }
  ],
  categories: [
    'ALL',
    'Symfony',
    'Api Platform',
    'Typescript',
    'React.js',
    'PostGreSQL',
    'VueJs',
    'DevOps',
    'Code Igniter',
    'Silex',
    'Ansible',
    'AngularJS',
    'Roots.io',
    'Wordpress',
    'Mysql'
  ].sort()
})

export const mutations = {
  filter(state, filter) {
    if (filter && filter !== 'ALL') {
      state.list.forEach(function(item) {
        item.visible = item.categories.indexOf(filter) > -1 ? 1 : 0
      })
    } else {
      state.list.forEach(function(item) {
        item.visible = 1
      })
    }
  }
}
