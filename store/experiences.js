export const state = () => ({
  list: [
    {
      dateStart: 'Novembre 2020',
      dateEnd: "Aujourd'hui",
      company: 'Université Claude Bernard Lyon 1',
      sector: '',
      link: 'https://iut.univ-lyon1.fr/formation/offre-de-formations/informatique-villeurbanne-doua/dut-informatique',
      city: 'Lyon',
      role: 'Enseignant vacataire | PHP Avancé | Symfony',
      description: [
        "Dans le cadre du DUT Informatique en 2ieme année, j'interviens dans l’unité d'enseignement " +
          "'UE31 : Informatique avancée' - 'Programmation Web côté serveur'"
      ],
      tags: ['Formation', 'Symfony', 'PHP', 'Git']
    },
    {
      dateStart: 'Février 2019',
      dateEnd: "Aujourd'hui",
      company: 'Université Claude Bernard Lyon 1',
      sector: '',
      link:
        'https://iut.univ-lyon1.fr/formation/offre-de-formations/informatique-villeurbanne-doua-/licence-professionnelle-metiers-de-l-informatique-conception-developpement-et-test-de-logiciels-parcours-developpeur-d-application-d-entreprise-administrateur-de-systemes-d-information-devops--602773.kjsp',
      city: 'Lyon',
      role: 'Enseignant vacataire | DevOps | Ansible',
      description: [
        "Dans le cadre de la Licence Professionnelle DEVOPS, j'interviens dans l’unité d'enseignement 'UE1 : " +
          "Compétence de Base' sur le module : 'Test, automatisation, déploiement' avec la technologie Ansible'"
      ],
      tags: [
        'Formation',
        'Ansible',
        'DevOps',
        'Openstack',
        'Git',
        'Virtualisation',
        'Administration Système',
        'Education Populaire'
      ]
    },
    {
      dateStart: 'Octobre 2012',
      dateEnd: "Aujourd'hui",
      company: 'Freelance',
      sector: '',
      link: '#',
      city: 'Lyon',
      role: '🚀 Développeur php | Symfony | Fullstack | Devops',
      description: [
        "Je suis pour l'essentiel un développeur Fullstack avec une vision DevOps." +
          "La polyvalence me permet d'être force de propositions tout en maîtrisant les différents aspects d'un projet."
      ],
      tags: [
        'Symfony',
        'API Platform',
        'Material Design',
        'Ansible',
        'Méthode Agile',
        'Angular',
        'VueJs',
        'Facebook',
        'API'
      ]
    },
    {
      dateStart: 'Août 2014',
      dateEnd: 'Juin 2017',
      company: 'Comptoir des Plats',
      sector: 'Restauration',
      link: '#',
      city: 'Lyon',
      role: 'Fondateur - Startuppeur',
      description: [
        'Création de la plateforme – Symfony 2.8 – Material design – Bower/Grunt - Vagrant / Ansible',
        'Finaliste à <a class="link font-weight-medium" href="https://www.lyonstartup.com/" target="_blank">Lyon Start Up</a>',
        'Gestion de projet entrepreneurial',
        'Relation clients et partenaires'
      ],
      tags: [
        'Symfony 2',
        'Material Design',
        'Grunt',
        'Bower',
        'Ansible',
        'Google Analytics',
        'Google Tag Manager',
        'Méthode Agile',
        'Stratégie de communication',
        'Commercial',
        "Création d'entreprise",
        "Stratégie d'entreprise",
        'Facebook',
        'API',
        'Start-up'
      ]
    },
    {
      dateStart: 'Janvier 2014',
      dateEnd: 'Juillet 2014',
      company: 'PrestaConcept',
      sector: 'Agence & SSII',
      link: 'https://www.prestaconcept.net',
      city: 'Lyon',
      role: 'Chef de projet - Scrum Master',
      description: [
        'Gestion de projets Agile, Estimations de projet, User Story, Sprint, Planification, ScrumMaster',
        "Gestion d'équipe",
        'Déploiement, Tests fonctionnel, Retour clients',
        'Présentation de projets, Réflexion autour de solutions applicatif mobile / web, Documentation, Spécification',
        'Projets : lyonaeroports.com, Bpeek.com, Auto-IES, Rateitgreen'
      ],
      tags: [
        'Gestion de projet Web',
        'Méthode Agile',
        'Scrum',
        'Kanban',
        'Spécifications fonctionnelles',
        'Spécifications techniques',
        "Management d'équipe"
      ]
    },
    {
      dateStart: 'Avril 2008',
      dateEnd: 'Décembre 2013',
      company: 'PrestaConcept',
      sector: 'Agence & SSII',
      link: 'https://www.prestaconcept.net',
      city: 'Lyon',
      role: 'Développeur Symfony',
      description: [
        "Frontend, Backend, Webservice, Intégration d'application tiers, Relation partenaire",
        'Développement Symfony 1, Silex : FantastikTV, Chiligaming, Bally Technology',
        'Formateur Web/Symfony 1 en Inde pour Bally Technology (3 semaines)',
        'Documentation, Spécification'
      ],
      tags: [
        'Symfony',
        'Javascript',
        'Prototype',
        'JQuery',
        'MySql',
        'Git',
        'Intégration Web',
        'Développement Web',
        'Intégration Continue',
        'Formation',
        'Partenariat'
      ]
    }
  ]
})

export const mutations = {}
