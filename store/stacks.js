export const state = () => ({
  listSVG: [
    { name: 'Symfony', type: 'Symfony2', link: 'https://symfony.com/' },
    { name: 'Angular', type: 'angular', link: 'https://angular.io/' },
    { name: 'Ngnix', type: 'Nginx_logo', link: 'https://nginx.org/' },
    { name: 'Git', type: 'Git-logo', link: 'https://git-scm.com/' },
    {
      name: 'Vagrant',
      type: 'Vagrant_VerticalLogo',
      link: 'https://www.vagrantup.com/'
    },
    { name: 'Ansible', type: 'Ansible_logo', link: 'http://ansible.com/' },
    {
      name: 'Api-Plaftorm',
      type: 'api-platform_logo',
      link: 'https://api-platform.com'
    },
    {
      name: 'VueJs',
      type: 'Vue_logo',
      link: 'https://vuejs.org/'
    }
  ].sort(function() {
    return 0.5 - Math.random()
  }),
  list: [
    {
      name: 'Angular',
      categories: ['Framework', 'JS', 'Frontend'],
      visible: 1,
      img: 'angularjs.png',
      value: 60,
      color: '#E62537'
    },
    {
      name: 'Ansible',
      categories: ['DevOps'],
      visible: 1,
      img: 'ansible.png',
      value: 90,
      color: '#000000'
    },
    {
      name: 'Ansible-tower',
      categories: ['DevOps'],
      visible: 1,
      img: 'ansible-tower.png',
      value: 90,
      color: '#CC0000'
    },
    {
      name: 'Apache',
      categories: ['DevOps'],
      visible: 1,
      img: 'apache.jpg',
      value: 90,
      color: '#D1149D'
    },
    {
      name: 'Api-Plateform',
      categories: ['Framework', 'Php', 'Backend'],
      visible: 1,
      img: 'api-plateform.png',
      value: 75,
      color: '#83CCD3'
    },
    {
      name: 'Atom',
      categories: ['IDE', 'Outils'],
      visible: 1,
      img: 'atom.png',
      value: 50,
      color: '#66595C'
    },
    {
      name: 'Bitbucket',
      categories: ['Git', 'DevOps'],
      visible: 1,
      img: 'Bitbucket.png',
      value: 85,
      color: '#2185FF'
    },
    {
      name: 'Boostrap',
      categories: ['Framework', 'Frontend', 'CSS', 'JS'],
      visible: 1,
      img: 'bootstrap.png',
      value: 90,
      color: '#4C3372'
    },
    {
      name: 'Bower',
      categories: ['Package Manager', 'CSS', 'JS', 'DevOps'],
      visible: 1,
      img: 'bower.png',
      value: 90,
      color: '#F05631'
    },
    {
      name: 'CodeIgniter',
      categories: ['Framework', 'Php', 'Backend'],
      visible: 1,
      img: 'codeIgniter.png',
      value: 90,
      color: '#550000'
    },
    {
      name: 'Composer',
      categories: ['Package Manager', 'Php', 'DevOps'],
      visible: 1,
      img: 'composer.png',
      value: 90,
      color: '#1A3D8A'
    },
    {
      name: 'Datagrip',
      categories: ['Database', 'Outils'],
      visible: 1,
      img: 'datagrip-icon.png',
      value: 90,
      color: '#30CD98'
    },
    {
      name: 'Debian',
      categories: ['DevOps'],
      visible: 1,
      img: 'debian.png',
      value: 60,
      color: '#D81717'
    },
    {
      name: 'Doctrine',
      categories: ['Php', 'Database'],
      visible: 1,
      img: 'doctrine.png',
      value: 90,
      color: '#F47B27'
    },
    {
      name: 'Elasticsearch',
      categories: ['DevOps', 'Database'],
      visible: 1,
      img: 'elasticsearch.png',
      value: 70,
      color: '#3BBFB3'
    },
    {
      name: 'FacebookApi',
      categories: ['API'],
      visible: 1,
      img: 'facebookApi.png',
      value: 90,
      color: '#385999'
    },
    {
      name: 'Git',
      categories: ['Git', 'DevOps'],
      visible: 1,
      img: 'git.png',
      value: 90,
      color: '#F04F33'
    },
    {
      name: 'Github',
      categories: ['Git', 'DevOps'],
      visible: 1,
      img: 'github.png',
      value: 90,
      color: '#000000'
    },
    {
      name: 'Gitlab',
      categories: ['Git', 'DevOps'],
      visible: 1,
      img: 'gitlab.png',
      value: 90,
      color: '#E34124'
    },
    {
      name: 'Google Tag Manager',
      categories: ['Outils', 'Analytics'],
      visible: 1,
      img: 'google_tag_manager.png',
      value: 60,
      color: '#C6C6C6'
    },
    {
      name: 'Google Maps API',
      categories: ['API', 'Outils'],
      visible: 1,
      img: 'googleMaps.png',
      value: 75,
      color: '#14A461'
    },
    {
      name: 'GraphQL',
      categories: ['API'],
      visible: 1,
      img: 'graphQL.png',
      value: 50,
      color: '#E535AB'
    },
    {
      name: 'Grunt',
      categories: ['JS', 'DevOps', 'CSS'],
      visible: 1,
      img: 'grunt.png',
      value: 60,
      color: '#BC7D0F'
    },
    {
      name: 'Gulp',
      categories: ['JS', 'DevOps', 'CSS'],
      visible: 1,
      img: 'gulp.png',
      value: 60,
      color: '#E94949'
    },
    {
      name: 'HTML5',
      categories: ['Frontend'],
      visible: 1,
      img: 'html5.png',
      value: 90,
      color: '#E54C21'
    },
    {
      name: 'IntelliJIDEA',
      categories: ['IDE', 'Outils'],
      visible: 1,
      img: 'intelliJIDEA.png',
      value: 90,
      color: '#177DE6'
    },
    {
      name: 'Ionic',
      categories: ['Framework', 'Frontend', 'JS'],
      visible: 1,
      img: 'Ionic.png',
      value: 70,
      color: '#3E8DF9'
    },
    {
      name: 'Javascript',
      categories: ['JS', 'Langage'],
      visible: 1,
      img: 'javascript.jpeg',
      value: 90,
      color: '#F0DB4E'
    },
    {
      name: 'Jenkins',
      categories: ['DevOps'],
      visible: 1,
      img: 'jenkins.png',
      value: 60,
      color: '#F0D7B8'
    },
    {
      name: 'Jira',
      categories: ['Gestion'],
      visible: 1,
      img: 'jira.png',
      value: 50,
      color: '#184E81'
    },
    {
      name: 'Jquery',
      categories: ['JS', 'Frontend'],
      visible: 1,
      img: 'jquery.png',
      value: 90,
      color: '#0869B0'
    },
    {
      name: 'Kanboard',
      categories: ['Gestion', 'Outils'],
      visible: 1,
      img: 'kanboard.png',
      value: 80,
      color: '#226B80'
    },
    {
      name: 'Less',
      categories: ['CSS', 'Frontend'],
      visible: 1,
      img: 'less.png',
      value: 80,
      color: '#20457B'
    },
    {
      name: 'LetEncrypt',
      categories: ['DevOps'],
      visible: 1,
      img: 'letEncrypt.png',
      value: 80,
      color: '#173B6B'
    },
    {
      name: 'MariaDB',
      categories: ['Database'],
      visible: 1,
      img: 'mariaDB.png',
      value: 90,
      color: '#10376E'
    },
    {
      name: 'Markdown',
      categories: ['Outils', 'Langage'],
      visible: 1,
      img: 'markdown.png',
      value: 75,
      color: '#151911'
    },
    {
      name: 'Material Design',
      categories: ['Frontend', 'CSS', 'JS'],
      visible: 1,
      img: 'materialDesign.png',
      value: 75,
      color: '#FFAD00'
    },
    {
      name: 'Materialize',
      categories: ['Framework', 'Frontend', 'CSS', 'JS'],
      visible: 1,
      img: 'Materialize.png',
      value: 80,
      color: '#EC7078'
    },
    {
      name: 'Matomo',
      categories: ['Analytics', 'Outils', 'DevOps'],
      visible: 1,
      img: 'matomo.png',
      value: 60,
      color: '#D52419'
    },
    {
      name: 'Mysql',
      categories: ['Database'],
      visible: 1,
      img: 'mysql.png',
      value: 90,
      color: '#57849F'
    },
    {
      name: 'Nginx',
      categories: ['DevOps'],
      visible: 1,
      img: 'nginx.png',
      value: 80,
      color: '#009E00'
    },
    {
      name: 'Node',
      categories: ['Backend', 'Frontend', 'JS'],
      visible: 1,
      img: 'node.png',
      value: 65,
      color: '#80BC01'
    },
    {
      name: 'Npm',
      categories: ['DevOps', 'Package Manager'],
      visible: 1,
      img: 'npm.png',
      value: 65,
      color: '#CC3433'
    },
    {
      name: 'Nuxt',
      categories: ['Framework', 'JS'],
      visible: 1,
      img: 'nuxt.png',
      value: 60,
      color: '#31475E'
    },
    {
      name: 'Php',
      categories: ['Php', 'Backend', 'Langage'],
      visible: 1,
      img: 'php.png',
      value: 95,
      color: '#6280B8'
    },
    {
      name: 'PhpStorm',
      categories: ['IDE', 'Outils'],
      visible: 1,
      img: 'phpStorm.png',
      value: 90,
      color: '#775AF8'
    },
    {
      name: 'Postgre',
      categories: ['Database'],
      visible: 1,
      img: 'postgre.png',
      value: 90,
      color: '#2E6691'
    },
    {
      name: 'Postman',
      categories: ['Outils, API'],
      visible: 1,
      img: 'postman.png',
      value: 90,
      color: '#FD6C35'
    },
    {
      name: 'Sass',
      categories: ['CSS', 'Frontend'],
      visible: 1,
      img: 'sass.png',
      value: 65,
      color: '#D370A3'
    },
    {
      name: 'Swagger',
      categories: ['API', 'Framework', 'Outils'],
      visible: 1,
      img: 'swagger.png',
      value: 60,
      color: '#6D9B00'
    },
    {
      name: 'Symfony',
      categories: ['Framework', 'Php', 'Backend'],
      visible: 1,
      img: 'symfony.png',
      value: 95,
      color: '#000000'
    },
    {
      name: 'Trello',
      categories: ['Gestion', 'Outils'],
      visible: 1,
      img: 'trello.png',
      value: 90,
      color: '#0080C9'
    },
    {
      name: 'TypeScript',
      categories: ['JS', 'Langage'],
      visible: 1,
      img: 'typeScript.jpg',
      value: 65,
      color: '#2875C3'
    },
    {
      name: 'Ubuntu',
      categories: ['DevOps'],
      visible: 1,
      img: 'ubuntu.jpg',
      value: 90,
      color: '#DE4815'
    },
    {
      name: 'Vagrant',
      categories: ['DevOps'],
      visible: 1,
      img: 'Vagrant.png',
      value: 90,
      color: '#2890CB'
    },
    {
      name: 'VueJs',
      categories: ['Framework', 'JS', 'Frontend'],
      visible: 1,
      img: 'vuejs.png',
      value: 60,
      color: '#3EB883'
    },
    {
      name: 'Vuetify',
      categories: ['Framework', 'Frontend', 'CSS', 'JS'],
      visible: 1,
      img: 'vuetify.jpg',
      value: 80,
      color: '#7DC5FF'
    },
    {
      name: 'WebStorm',
      categories: ['IDE', 'Outils'],
      visible: 1,
      img: 'webStorm.png',
      value: 90,
      color: '#00CED8'
    },
    {
      name: 'Wordpress',
      categories: ['Php', 'CMS'],
      visible: 1,
      img: 'wordpress.png',
      value: 80,
      color: '#D64E1D'
    },
    {
      name: 'Yarn',
      categories: ['DevOps', 'Package Manager'],
      visible: 1,
      img: 'yarn.jpg',
      value: 80,
      color: '#2F8CBF'
    },
    {
      name: 'Yii',
      categories: ['Framework', 'Php', 'Backend'],
      visible: 1,
      img: 'yii.png',
      value: 90,
      color: '#B7D387'
    }
  ],
  categories: [
    'API',
    'ALL',
    'Framework',
    'CMS',
    'Gestion',
    'Langage',
    'Php',
    'Database',
    'CSS',
    'JS',
    'DevOps',
    'Backend',
    'Frontend',
    'Package Manager',
    'Git',
    'Outils',
    'Analytics',
    'IDE'
  ].sort()
})

export const mutations = {
  filterStacks(state, filter) {
    if (filter && filter !== 'ALL') {
      state.list.forEach(function(stack) {
        stack.visible = stack.categories.indexOf(filter) > -1 ? 1 : 0
      })
    } else {
      state.list.forEach(function(stack) {
        stack.visible = 1
      })
    }
  }
}
