export const state = () => ({
  list: [
    'Je suis un développeur backend',
    'Je suis un développeur Symfony',
    'Je suis un développeur PHP',
    'Je suis un développeur frontend',
    'Je suis un développeur fullstack',
    'Je suis un développeur indépendant',
    'Je suis devops',
    'Je suis agile',
    'Je suis un artisan du Web'
  ]
})

export const getters = {
  getWhoIAm: state => state.list
}
