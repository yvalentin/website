// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
// const pkg = require('./package')
require('dotenv').config()
const _ = require('lodash')

const glob = require('glob')
const files = glob.sync('**/*.md', { cwd: 'content' })

// We define a function to trim the '.md' from the filename
// and return the correct path.
// This function will be used later
function getSlugs(post) {
  if (!_.isEmpty(post)) {
    const slug = post.substr(0, post.lastIndexOf('.'))
    return `/articles/${slug}`
  }
}

module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate:
      '%s - Symfony - Devops - Yohann Valentin - Indépendant - Lyon',
    title: 'Accueil',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#ffffff' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Yohann Valentin - Indépendant - Expert Symfony - PHP - Devops - Lyon'
      }
    ],
    noscript: [{ innerHTML: 'This website requires JavaScript.' }],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
        size: '32x32'
      },
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
        size: '16x16'
      },
      { rel: 'apple-touch-icon', href: '/apple-touch-icon.png', size: '76x76' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', color: '#5bbad5', href: '/safari-pinned-tab.svg' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
   ** Customize the generated output folder
   */
  // generate: {
  //   routes: function() {
  //     return files.map(getSlugs())
  //   }
  // },
  // router: {
  //   base: '/website/'
  // },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#DF7452' },

  /*
   ** Global CSS
   */
  css: [
    '~/assets/style/app.styl',
    'vue-typed-js/dist/vue-typed-js.css',
    'prismjs/themes/prism-okaidia.css',
    '~/assets/fontello/css/fontello.css'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-particules.js', ssr: false },
    { src: '~/plugins/vue-typed.js' },
    // '@plugins/prism',
    '~/plugins/vuetify.js'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // 'nuxt-express-module',
    '@nuxtjs/axios',
    '@dinamomx/nuxtent',
    '@nuxtjs/sitemap'
    // '@nuxtjs/dotenv',
    // [
    //   'nuxt-validate',
    //   {
    //     lang: 'fr'
    //   }
    // ]
  ],
  /*
   ** Axios module configuration
   */
  // axios: {
  //   baseURL: 'http://localhost:3000'
  // },

  /*
   ** Build configuration
   */
  build: {
    // transpile: ['vuetify/lib'],
    // plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      // stylus: {
      //   import: ['~/assets/style/variables.styl']
      // }
    },
    // extractCSS: true,
    /*
     ** Run ESLint on save
     */
    extend(config, ctx) {
      // extend for svg
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
      svgRule.test = /\.(png|jpe?g|gif|webp)$/
      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader'
      })

      // Loader markdown
      config.module.rules.push({
        test: /\.md$/,
        use: ['raw-loader']
      })
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        // config.module.rules.push({
        //   enforce: 'pre',
        //   test: /\.(js|vue)$/,
        //   loader: 'eslint-loader',
        //   exclude: /(node_modules)/
        // })
      }
    }
  }
}
