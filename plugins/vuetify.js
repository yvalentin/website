import '@fortawesome/fontawesome-free/scss/fontawesome.scss'
import '@fortawesome/fontawesome-free/scss/solid.scss'
import '@fortawesome/fontawesome-free/scss/regular.scss'
import '@fortawesome/fontawesome-free/scss/brands.scss'
import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'fa',
  options: {
    customProperties: true
  },
  theme: {
    primary: '#df7452',
    accent: '#43615C',
    secondary: '#c08ec1',
    info: colors.teal.lighten1,
    warning: '#e0bd60',
    error: colors.deepOrange.accent4,
    success: colors.green.accent3
  }
})
