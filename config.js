let config = {
  smtpConfig: {
    host: process.env.smtpHost,
    port: process.env.smtpPort,
    secure: true,
    auth: {
      user: process.env.smtpUser,
      pass: process.env.smtpPass
    },
    tls: {
      rejectUnauthorized: false
    }
  },
  sendTo: process.env.smtpSendTo,
  subject: '[yohannvalentin.com] Contact form'
}

module.exports = config
