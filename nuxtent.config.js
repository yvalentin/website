// const Prism = require('prismjs')
const prism = require('markdown-it-prism');
module.exports = {
  markdown: {
    plugins: [
      prism
    ]
    //   extend(config) {
    //     config.highlight = (code, lang) => {
    //       return `<pre class="language-${lang}"><code class="language-${lang}">${Prism.highlight(
    //         code,
    //         Prism.languages[lang] || Prism.languages.markup
    //       )}</code></pre>`
    //     }
    //   }
  },
  content: [
    [
      'articles',
      {
        page: '/articles/_slug',
        permalink: '/articles/:slug',
        isPost: false,
        generate: ['get', 'getAll']
      }
    ]
  ],
  api: {
    baseURL:
      process.env.NODE_ENV === 'production'
        ? 'https://yohannvalentin.com'
        : 'http://localhost:3000'
  },
}
